@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCompanyModal">
                    Add Company
                </button>
            </div>
        </div>
        <table id="companyTable" class="display">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Logo</th>
                    <th>Website</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- Add Company Modal -->
    <div class="modal fade" id="addCompanyModal" tabindex="-1" role="dialog" aria-labelledby="addCompanyModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addCompanyModalLabel">Add Company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addCompanyForm" action="{{route('companies.store')}}">
                        @csrf <!-- {{ csrf_field() }} -->
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="logo">Logo</label>
                            <input type="file" accept="image/png, image/gif, image/jpeg"  class="form-control-file" id="logo" name="logo">
                        </div>
                        <div class="form-group">
                            <label for="website">Website</label>
                            <input type="text" class="form-control" id="website" name="website">
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Company Modal -->
    <div class="modal fade" id="editCompanyModal" tabindex="-1" role="dialog" aria-labelledby="editCompanyModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editCompanyModalLabel">Edit Company</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editCompanyForm" action="{{route('companies.store')}}">
                        @csrf <!-- {{ csrf_field() }} -->
                        <input type="hidden" name="company_id" id="company_id">
                        <div class="form-group">
                            <label for="edit_name">Name</label>
                            <input type="text" class="form-control" id="edit_name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="edit_email">Email</label>
                            <input type="email" class="form-control" id="edit_email" name="email">
                        </div>

                        <div class="form-group">
                            <img id="logo_view" src="" width="100" height="100" alt="Insert link 1 alt text here" />
                            <label for="edit_logo"> Logo</label>
                            <input type="file" accept="image/png, image/gif, image/jpeg"  class="form-control-file" id="edit_logo" name="logo">
                        </div>
                        <div class="form-group">
                            <label for="edit_website">Website</label>
                            <input type="text" class="form-control" id="edit_website" name="website">
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function() {
            // Initialize DataTable with search and pagination
            var companyTable = $('#companyTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('companies.index') }}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    {
                        data: 'logo',
                        name: 'logo',
                        render: function(data, type, full, meta) {
                            if (data) {
                                    //  alert("{{  url('/storage/logos')}}" + '/' + data)
                                return '<img src="{{  url('/storage/logos')}}/' + data + '"  width="100" height="100" alt="Company Logo" >';
                            } else {
                                return '';
                            }
                        }
                    },
                    { data: 'website', name: 'website' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "pageLength": 10
            });

            // Add Company form submission using AJAX
            $('#addCompanyForm').submit(function(e) {
                e.preventDefault();

                var form = $(this);
                var url = form.attr('action');
                var formData = new FormData(form[0]);

                var resource = $(this).data("resource");



                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.success) {
                            alert(response.message);
                            companyTable.ajax.reload();
                            $('#addCompanyModal').modal('hide');


                            form.trigger('reset');
                        } else {
                            alert('Error adding company. Please check input.');
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Error adding company. Please try again.');
                    }
                });
            });

            // Edit Company form submission using AJAX
            $('#editCompanyForm').submit(function(e) {
                e.preventDefault();

                var form = $(this);
                var url = form.attr('action');
                var formData = new FormData(form[0]);

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.success) {
                            alert(response.message);
                            companyTable.ajax.reload();
                            $('#editCompanyModal').modal('hide');
                            form.trigger('reset');
                        } else {
                            alert('Error updating company. Please try again.');
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Error updating company. Please try again.');
                    }
                });
            });




            $('body').on('click', '.editPost', function () {

      var id = $(this).data('id');
      $.get("{{ route('companies.index') }}" +'/' + id +'/edit', function (data) {
        //alert("{{  url('/storage/logos')}}" + '/' + data.logo)
        $("#logo_view").attr("src","{{  url('/storage/logos')}}" + '/' + data.logo);
        $('#edit_website').val(data.website);
          $('#modelHeading').html("Edit Post");
          $('#savedata').val("edit-user");
          $('#editCompanyModal').modal('show');
          $('#company_id').val(data.id);
          $('#edit_name').val(data.name);
          $('#edit_email').val(data.email);
          $('#edit_logo').val(data.logo);




      })
   });





   $('body').on('click', '.deletePost', function () {

     var id = $(this).data("id");
     var url = "{{ route('companies.destroy', ":id") }}";
     url = url.replace(':id', id);

     confirm("Are You sure want to delete !");

     $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

     $.ajax({
         type: 'delete',
         data: { "id": id},
         datatype : 'json',
         url: url,
         success: function (data) {
            $('#companyTable').DataTable().ajax.reload();
         },
         error: function (data) {
             alert('Error:', data);
         }
     });
 });



        });
    </script>
@endsection
