<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

use Illuminate\Support\Facades\Storage;
use DataTables;

class CompanyController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $companies = Company::all();
            return DataTables::of($companies)
                ->addColumn('action', function ($company) {
                    $editUrl = route('companies.edit', $company->id);
                    $deleteUrl = route('companies.destroy', $company->id);
                    $csrfToken = csrf_token();

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$company->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editPost">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$company->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deletePost">Delete</a>';
                    return $btn;
                    //  "<a href='#' data-toggle='modal' data-target='#addCompanyModal'
                    // data-id=$company->id


                    // class='btn btn-sm btn-primary'>Edit</a>
                    //         <button type='button' class='btn btn-sm btn-danger' onclick='deleteCompany({$company->id}, \"{$deleteUrl}\", \"{$csrfToken}\")'>Delete</button>";
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('companies.index');
    }

    public function store(Request $request)
    {    logger($request->all());

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'nullable|email',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:204888',
            'website' => 'nullable',
        ]);


        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $logoPath = $logo->store('public/logos');
            $validatedData['logo'] = str_replace("public/logos/", "", $logoPath);
        }
logger($validatedData);

        $company = Company::updateOrCreate( ['id' => $request->company_id] , $validatedData);

        return response()->json(['success' => true, 'message' => 'success.', 'company' => $company]);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'nullable|email',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'website' => 'nullable',
        ]);

        $company = Company::findOrFail($id);

        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $logoPath = $logo->store('public/logos');
            $validatedData['logo'] = $logoPath;
        }

        $company->update($validatedData);

        return response()->json(['success' => true, 'message' => 'Company updated successfully.']);
    }



    public function edit($id)
    {
        $post = Company::findOrFail($id);
        return response()->json($post);
    }

    public function destroy($id)
    {   logger($id);
        $company = Company::findOrFail($id);
        $company->delete();

        return response()->json(['success' => true, 'message' => 'Company deleted successfully.']);
    }
}

